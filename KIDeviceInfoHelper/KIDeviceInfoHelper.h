//
//  KIDeviceInfoHelper.h
//  KIDeviceInfoHelper
//
//  Created by Kazuyuki Imada on 2014/11/02.
//  Copyright (c) 2014年 Kazuyuki Imada. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG

#define KID_DEBUG(format, ...) NSLog([NSString stringWithFormat:@"[KID-DEBUG]\n%s:[%d]\n%@", __PRETTY_FUNCTION__, __LINE__, (format)], __VA_ARGS__)
#define KID_DEBUG_HERE NSLog(@"[KID-DEBUG] here: %s:[%d]", __PRETTY_FUNCTION__, __LINE__)

#else

#define KID_DEBUG(x...) ;
#define KID_DEBUG_HERE ;

#endif

#define kKIDeviceModelDetailNamesName @"name"
#define kKIDeviceModelDetailNamesShortName @"shortname"



@interface KIDeviceInfoHelper : NSObject

+ (NSString *)platformName;

+ (NSString *)modelDetailName;
+ (NSString *)modelDetailNameForPlatform:(NSString *)platform;

+ (NSString *)modelDetailShortName;
+ (NSString *)modelDetailShortNameForPlatform:(NSString *)platform;

+ (NSDictionary *)modelDetailNameDictForPlatform:(NSString *)platform;

@end

@interface NSDictionary(KIDeviceInfoHelper)

- (NSString *)modelDetailName;

- (NSString *)modelDetailShortName;

@end
