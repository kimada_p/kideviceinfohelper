//
//  KIDeviceInfoHelper.m
//  KIDeviceInfoHelper
//
//  Created by Kazuyuki Imada on 2014/11/02.
//  Copyright (c) 2014年 Kazuyuki Imada. All rights reserved.
//

#import "KIDeviceInfoHelper.h"
#include <sys/sysctl.h>


static NSDictionary *__modelDetailNameForPlatform;

@interface KIDeviceInfoHelper()

+ (NSString *)__ctlhw:(int)param;

@end


@implementation KIDeviceInfoHelper

+ (void)initialize {
    
    
    NSBundle *thisBundle = [NSBundle bundleForClass:[self class]];
    NSString *path = [thisBundle pathForResource:@"KIDeviceModelDetailNames" ofType:@"plist"];
    __modelDetailNameForPlatform = [[NSDictionary alloc] initWithContentsOfFile:path];
    
}


+ (NSString *)platformName {
    return [[self class] __ctlhw:HW_MACHINE];
}

+ (NSString *)modelDetailName {
    
    return [self modelDetailNameForPlatform:[self platformName]];

}

+ (NSString *)modelDetailNameForPlatform:(NSString *)platform {
    NSDictionary *modelDetailNameDict = [self modelDetailNameDictForPlatform:platform];
    
    if (modelDetailNameDict == nil) {
        return platform;
    }
    
    return modelDetailNameDict.modelDetailName;
}

+ (NSString *)modelDetailShortName {
    
    return [self modelDetailShortNameForPlatform:[self platformName]];
    
}

+ (NSString *)modelDetailShortNameForPlatform:(NSString *)platform {
    
    NSDictionary *modelDetailNameDict = [self modelDetailNameDictForPlatform:platform];
    
    if (modelDetailNameDict == nil) {
        return platform;
    }
    
    return modelDetailNameDict.modelDetailShortName;
}




+ (NSDictionary *)modelDetailNameDictForPlatform:(NSString *)platform {
    
    NSDictionary *retVal = [__modelDetailNameForPlatform objectForKey:platform];
    KID_DEBUG(@"modelDetailNameDict:[%@]%@", platform, retVal);
    return retVal;
    
}

+ (NSString *)__ctlhw:(int)param {
    size_t length;
    int mib[2];
   	mib[0] = CTL_HW;
    mib[1] = param;
    char *p;
    if (sysctl(mib, 2, NULL, &length, NULL, 0) < 0) {
        NSLog(@"%s(): Error in sysctl(): %s",
              __FUNCTION__, strerror(errno));
    }
    p = malloc(length);
    if (sysctl(mib, 2, p, &length, NULL, 0) < 0) {
        NSLog(@"%s(): Error in sysctl(): %s",
              __FUNCTION__, strerror(errno));
    }
    
    NSString *retVal = [NSString stringWithCString:p encoding:NSUTF8StringEncoding];
    free(p);
    return retVal;
}

@end

@implementation NSDictionary(KIDeviceInfoHelper)

- (NSString *)modelDetailName {
    return self[kKIDeviceModelDetailNamesName];
}

- (NSString *)modelDetailShortName {
    return self[kKIDeviceModelDetailNamesShortName];
}



@end

