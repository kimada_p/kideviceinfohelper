//
//  KIDeviceInfoHelperTest.m
//  KIDeviceInfoHelper
//
//  Created by Kazuyuki Imada on 2014/11/03.
//  Copyright (c) 2014年 Kazuyuki Imada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "KIDeviceInfoHelper.h"

@interface KIDeviceInfoHelperTest : XCTestCase

@end

@implementation KIDeviceInfoHelperTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void)testModelDetailNameForPlatform {
    XCTAssert([@"iPad Air 2 Wi-Fi + Cellular" isEqualToString:[KIDeviceInfoHelper modelDetailNameForPlatform:@"iPad5,4"]]);
}

- (void)testModelDetailNameFromDict {
    XCTAssert([@"iPad Air 2" isEqualToString:[KIDeviceInfoHelper modelDetailShortNameForPlatform:@"iPad5,4"]]);
}


#if TARGET_IPHONE_SIMULATOR

- (void)testPlatformName {
    XCTAssert([@"x86_64" isEqualToString:[KIDeviceInfoHelper platformName]]);
}

- (void)testModelDetailName {
    XCTAssert([@"iOS Simulator (x86_64)" isEqualToString:[KIDeviceInfoHelper modelDetailName]]);
}

- (void)testModelDetailShortName {
    XCTAssert([@"iOS Simulator" isEqualToString:[KIDeviceInfoHelper modelDetailShortName]]);
}


#endif

@end
