# KIDeviceInfoHelper #
KIDeviceInfoHelper allows you to get the model name of iOS device which your app is running.

### How do I get set up? ###

* Drag the KIDeviceInfoHelper/KIDeviceInfoHelper folder into your project.

### Usage ###

```
#!objective-c
    //
    // Getting the platform name, e.g. iPad5,4
    //
    [KIDeviceInfoHelper platformName];

    //
    // Getting the model detail name, e.g. iPad Air 2 Wi-Fi + Cellular
    //
    [KIDeviceInfoHelper modelDetailName];

    //
    // Getting the model detail SHORT name, e.g. iPad Air 2
    //
    [KIDeviceInfoHelper modelDetailShortName];

```
# Note: About model detail names #
Model detail names defined by KIDeviceInfoHelper are based on the Download Links of iOS image files in iOS Dev Center. It's useful for iOS developers to match their device with the Download Links. 
If you don't like this, you can replace the each model detail name by editing KIDeviceModelDetailNames.plist.